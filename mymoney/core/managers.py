from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce


class BillingCycleManager(models.Manager):
    def summary(self):
        result = (
            super()
            .get_queryset()
            .aggregate(
                total_debit=Coalesce(Sum("debits__value"), 0),
                total_credit=Coalesce(Sum("credits__value"), 0),
            )
        )

        return result
