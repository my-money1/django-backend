from django.http import JsonResponse

from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework import status

# TODO: Refactor to parse value when the type is dict
def _prepare_validation_errors(response_data):
    errors = []
    for field, value in response_data.items():
        errors.append(f"{field}: {''.join(value)}")

    return errors


def _handle_validation_error(response):
    # Now add the HTTP status code to the response.
    if response is not None:
        orig_data = response.data

        response.data = {}
        errors = _prepare_validation_errors(orig_data)

        response.data = {"errors": errors}

    return response


def not_found(request, exception):
    """
    Generic 404 error handler.
    """
    data = {"errors": ["Not Found (404)"]}
    return JsonResponse(data, status=status.HTTP_400_BAD_REQUEST)


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    response = _handle_validation_error(response)

    if response is None:
        response = Response({"errors": [exc.args[0]]})

    return response
