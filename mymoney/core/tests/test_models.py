import pytest

from mymoney.core.models import Credit, Debit, DebitStatusChoice, BillingCycle


@pytest.mark.django_db
def test_credit_model(billing_model):
    credit = Credit(name="Debit Description", value=10.0, billing=billing_model)  # noqa
    credit.save()

    assert credit.name == "Debit Description"
    assert credit.value == 10.0


@pytest.mark.django_db
def test_debit_model(billing_model):
    debit = Debit(
        name="Debit Description",
        value=10.0,
        status=DebitStatusChoice.PAYED,
        billing=billing_model,
    )
    debit.save()

    assert debit.name == "Debit Description"
    assert debit.value == 10.0
    assert debit.status == DebitStatusChoice.PAYED


@pytest.mark.django_db
def test_billing_cycle_model():
    billing = BillingCycle(name="Billing Description", month=5, year=2019)
    billing.save()

    credit = Credit(name="Credit Description", value=10.0, billing=billing)
    credit.save()

    debit = Debit(
        name="Debit Description",
        value=10.0,
        status=DebitStatusChoice.PAYED,
        billing=billing,
    )
    debit.save()

    assert billing.name == "Billing Description"
    assert billing.month == 5
    assert billing.year == 2019
    assert billing.debits.get().name == debit.name
    assert billing.credits.get().name == credit.name


@pytest.mark.django_db
def test_fetch_summary_of_billing_cycles(billing_summary):
    summary = BillingCycle.objects.summary()

    assert float(summary["total_debit"]) == 15.0
    assert float(summary["total_credit"]) == 10.0