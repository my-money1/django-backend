import pytest

from model_mommy import mommy

from mymoney.core.models import BillingCycle, Credit, Debit


@pytest.fixture
@pytest.mark.django_db
def billing_list():
    return mommy.make(BillingCycle, month=1, year=2020, _quantity=5)


@pytest.fixture
@pytest.mark.django_db
def billing_summary():
    billing = mommy.make(BillingCycle, month=1, year=2020)
    mommy.make(Credit, value=10.0, billing=billing)
    mommy.make(Debit, value=15.0, billing=billing)

    return billing


@pytest.fixture
@pytest.mark.django_db
def billing_model():
    return mommy.make(BillingCycle, month=1, year=2020)


@pytest.fixture
@pytest.mark.django_db
def credit_model():
    return mommy.make(Credit, value=10.0)


@pytest.fixture
@pytest.mark.django_db
def debit_model():
    return mommy.make(Debit, value=15.0)
