import pytest

from rest_framework import status

from mymoney.core.models import BillingCycle


@pytest.mark.django_db
def test_list_billing_cycles(billing_model, client):
    resp = client.get(f"/api/billings")

    assert resp.status_code == status.HTTP_200_OK
    assert resp.data["results"][0]["name"] == billing_model.name


@pytest.mark.django_db
def test_detail_billing_cycle(billing_model, client):
    resp = client.get(f"/api/billings/{billing_model.pk}")

    assert resp.status_code == status.HTTP_200_OK
    assert resp.data["name"] == billing_model.name


@pytest.mark.django_db
def test_create_billing_cycle(client):
    billings = BillingCycle.objects.all()
    assert len(billings) == 0

    data = {
        "name": "Billing Created",
        "month": 1,
        "year": 2020,
        "debits": [
            {"name": "Debit Transaction", "value": 15.0, "status": "PENDING"}
        ],  # noqa
        "credits": [{"name": "Credit Transaction", "value": 22.0}],
    }

    resp = client.post(
        f"/api/billings", data=data, content_type="application/json",
    )  # noqa

    assert resp.status_code == status.HTTP_201_CREATED
    assert resp.data["name"] == "Billing Created"

    billings = BillingCycle.objects.all()
    assert len(billings) == 1

    assert billings.get().debits.count() == 1
    assert billings.get().credits.count() == 1


@pytest.mark.django_db
def test_create_billing_cycle_with_wrong_transaction_status(client):
    billings = BillingCycle.objects.all()
    assert len(billings) == 0

    data = {
        "name": "Billing Created",
        "month": 1,
        "year": 2020,
        "debits": [
            {"name": "Debit Transaction", "value": 15.0, "status": "OTHER"}
        ],  # noqa
        "credits": [],
    }

    resp = client.post(
        f"/api/billings", data=data, content_type="application/json",
    )  # noqa

    assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_billing_cycle_with_debit_transaction_negative(client):
    billings = BillingCycle.objects.all()
    assert len(billings) == 0

    data = {
        "name": "Billing Created",
        "month": 1,
        "year": 2020,
        "debits": [
            {"name": "Debit Transaction", "value": -15.0, "status": "PENDING"}
        ],  # noqa
        "credits": [],
    }

    resp = client.post(
        f"/api/billings", data=data, content_type="application/json",
    )  # noqa

    assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_billing_cycle_with_credit_transaction_negative(client):
    billings = BillingCycle.objects.all()
    assert len(billings) == 0

    data = {
        "name": "Billing Created",
        "month": 1,
        "year": 2020,
        "debits": [],
        "credits": [{"name": "Credit Transaction", "value": -15.0}],  # noqa
    }

    resp = client.post(
        f"/api/billings", data=data, content_type="application/json",
    )  # noqa

    assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_billing_cycle_with_month_negative(client):
    billings = BillingCycle.objects.all()
    assert len(billings) == 0

    data = {
        "name": "Billing Created",
        "month": -1,
        "year": 2020,
        "debits": [],
        "credits": [{"name": "Credit Transaction", "value": 15.0}],  # noqa
    }

    resp = client.post(
        f"/api/billings", data=data, content_type="application/json",
    )  # noqa

    assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_update_billing_cycle(billing_model, client):
    data = {
        "id": billing_model.pk,
        "name": "Billing Updated",
        "month": billing_model.month,
        "year": billing_model.year,
        "debits": [],
        "credits": []
    }

    resp = client.put(
        f"/api/billings/{billing_model.pk}",
        data=data,
        content_type="application/json",  # noqa
    )
    assert resp.status_code == status.HTTP_200_OK

    resp = client.get(f"/api/billings/{billing_model.pk}")
    assert resp.data["name"] == data["name"]


@pytest.mark.django_db
def test_delete_billing_cycle(billing_model, client):
    billings = BillingCycle.objects.all()
    assert len(billings) == 1

    resp = client.delete(f"/api/billings/{billing_model.pk}")

    assert resp.status_code == status.HTTP_204_NO_CONTENT

    billings = BillingCycle.objects.all()
    assert len(billings) == 0


@pytest.mark.django_db
def test_fetch_count_billing_cycles(billing_list, client):
    resp = client.get(f"/api/billings/count")

    assert resp.status_code == status.HTTP_200_OK
    assert resp.data["count"] == 5


@pytest.mark.django_db
def test_fetch_summary_billing_cycles(billing_summary, client):
    resp = client.get(f"/api/billings/summary")

    assert resp.status_code == status.HTTP_200_OK
    assert float(resp.data["total_debit"]) == 15.0
    assert float(resp.data["total_credit"]) == 10.0
