from rest_framework import serializers

from mymoney.core.models import BillingCycle, Debit, Credit


class CreditSerializer(serializers.ModelSerializer):
    value = serializers.IntegerField(min_value=0)

    class Meta:
        model = Credit
        fields = ("id", "name", "value",)
        read_only_fields = ("id",)


class DebitSerializer(serializers.ModelSerializer):
    value = serializers.IntegerField(min_value=0)

    class Meta:
        model = Debit
        fields = ("id", "name", "value", "status",)
        read_only_fields = ("id",)


class BillingCycleSerializer(serializers.ModelSerializer):
    month = serializers.IntegerField(min_value=1, max_value=12)
    year = serializers.IntegerField(min_value=1970, max_value=2100)
    debits = DebitSerializer(many=True)
    credits = CreditSerializer(many=True)

    class Meta:
        model = BillingCycle
        fields = "__all__"
        read_only_fields = ("id",)

    def _save_debits(self, debits, instance):
        for debit in debits:
            Debit.objects.create(billing=instance, **debit)

    def _save_credits(self, credits, instance):
        for credit in credits:
            Credit.objects.create(billing=instance, **credit)

    def create(self, validated_data):
        debits = validated_data.pop('debits')
        credits = validated_data.pop('credits')
        instance = super().create(validated_data)

        self._save_debits(debits, instance)
        self._save_credits(credits, instance)

        return instance

    def update(self, instance, validated_data):
        debits = validated_data.pop('debits')
        credits = validated_data.pop('credits')
        instance = super().update(instance, validated_data)

        self._save_debits(debits, instance)
        self._save_credits(credits, instance)

        return instance
