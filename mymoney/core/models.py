from django.db import models

from mymoney.core.managers import BillingCycleManager


class Credit(models.Model):
    billing = models.ForeignKey(
        "core.BillingCycle", related_name="credits", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=255)
    value = models.DecimalField(max_digits=8, decimal_places=2)


class DebitStatusChoice(models.TextChoices):
    PAYED = "PAYED"
    PENDING = "PENDING"
    SCHEDULED = "SCHEDULES"


class Debit(models.Model):
    billing = models.ForeignKey(
        "core.BillingCycle", related_name="debits", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=255)
    value = models.DecimalField(max_digits=8, decimal_places=2)
    status = models.CharField(max_length=50, choices=DebitStatusChoice.choices)


class BillingCycle(models.Model):
    name = models.CharField(max_length=255)
    month = models.IntegerField()
    year = models.IntegerField()

    objects = BillingCycleManager()
