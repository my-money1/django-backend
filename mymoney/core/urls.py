from django.urls import path

from mymoney.core.views import (
    BillingList,
    BillingDetail,
    BillingCount,
    BillingSummary,
)  # noqa

from mymoney.core.exceptions import not_found

urlpatterns = [
    path("billings", BillingList.as_view()),
    path("billings/<int:pk>", BillingDetail.as_view()),
    path("billings/count", BillingCount.as_view()),
    path("billings/summary", BillingSummary.as_view()),
]

handler500 = "rest_framework.exceptions.server_error"
handler400 = "rest_framework.exceptions.bad_request"
handler404 = not_found
