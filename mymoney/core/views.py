from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from mymoney.core.models import BillingCycle
from mymoney.core.serializers import BillingCycleSerializer


class BillingList(generics.ListCreateAPIView):
    queryset = BillingCycle.objects.all()

    serializer_class = BillingCycleSerializer


class BillingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = BillingCycle.objects.all()

    serializer_class = BillingCycleSerializer


class BillingCount(APIView):
    def get(self, request):
        total_billings = BillingCycle.objects.count()
        return Response({"count": total_billings})


class BillingSummary(APIView):
    def get(self, request):
        result = BillingCycle.objects.summary()
        return Response(result)
