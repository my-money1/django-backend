# Pytest Plugins
plugins = []

# Project Fixtures
fixtures = [
    "mymoney.core.tests.fixtures",
]

pytest_plugins = plugins + fixtures
