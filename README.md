# My Money API

Project that develop API of My Money

## Technologies
- Python
- Django
- Django REST Framework
- PostgreSQL

## Install

1. Create virtualenv
2. Install via Poetry: `poetry install`

## API Endpoints

![Endpoints](./endpoints.png)